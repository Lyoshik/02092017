import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;

public class MainController {

    @FXML
    Label label;

    @FXML
    TextField textField;

    @FXML
    private void handleButtonClick() {
        label.setText(textField.getText());
    }
}
